package net.aeronetwork.skyblock.island;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Maps;
import com.google.gson.GsonBuilder;
import net.aeronetwork.core.Core;
import net.aeronetwork.core.manager.Manager;
import net.aeronetwork.skyblock.Skyblock;
import net.aeronetwork.skyblock.island.commands.IslandCommand;
import net.aeronetwork.skyblock.user.IslandUser;
import net.minecraft.server.v1_8_R3.GameRules;
import org.apache.commons.lang3.Validate;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.WorldType;
import org.bukkit.craftbukkit.v1_8_R3.CraftWorld;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;

public class IslandManager extends Manager {

    private File islandsFolder;

    private World islandWorld;

    private HashMap<UUID, Island> islandCache;
    private HashMap<UUID, IslandUser> islandUsers;
    private HashMap<UUID, List<Island>> inviteCache;

    public IslandManager() {
        super("Island Manager", "Manages all Island related tasks.", Skyblock.getInstance(), Core.getInstance().getCommandManager());
        islandWorld = Bukkit.getWorld("island_world");
        if(islandWorld == null) {
            WorldCreator worldCreator = new WorldCreator("island_world");
            worldCreator.generateStructures(false);
            worldCreator.type(WorldType.FLAT);
            worldCreator.generatorSettings("2;0;1");
            islandWorld = worldCreator.createWorld();
            ((CraftWorld) islandWorld).getHandle().getWorldData().x().a("nextX", "0", GameRules.EnumGameRuleType.NUMERICAL_VALUE);
        }
    }

    @Override
    public void register() {
        islandCache = Maps.newHashMap();
        islandUsers = Maps.newHashMap();
        inviteCache = Maps.newHashMap();

        islandsFolder = new File(Skyblock.getInstance().getDataFolder() + "/islands");

        if(!islandsFolder.exists()) {
            islandsFolder.mkdirs();
            Bukkit.getLogger().log(Level.INFO, "[Island Manager]: Successfully created Islands folder.");
        } else {
            if(islandsFolder.listFiles().length != 0) {
                for(File file : islandsFolder.listFiles()) {
                    Island island = readIslandFile(file);
                    Validate.notNull(island, "[Island Manager]: Island cannot be null.");
                    Validate.notNull(islandCache, "[Island Manager]: Island cache cannot be null.");
                    islandCache.put(island.getOwner(), island);
                }
            }
        }
    }

    @Override
    public void registerCommands() {
        addCommand(new IslandCommand());
    }

    public Island readIslandFile(File file) {
        System.out.println(file.getAbsolutePath());
        Island island = null;

        try {
            island = new GsonBuilder().setPrettyPrinting().create().fromJson(new FileReader(file.getAbsolutePath()), Island.class);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        System.out.println(island.getOwner().toString());

        return island;
    }

    public File getIslandsFolder() {
        return islandsFolder;
    }

    public World getIslandWorld() {
        return islandWorld;
    }

    public int getNextIslandX() {
        int current = Integer.valueOf(getIslandWorld().getGameRuleValue("nextX"));
        getIslandWorld().setGameRuleValue("nextX", current + 400 + "");
        return current;
    }

    public HashMap<UUID, IslandUser> getIslandUsers() {
        return islandUsers;
    }

    public HashMap<UUID, Island> getIslandCache() {
        return islandCache;
    }

    public HashMap<UUID, List<Island>> getInviteCache() {
        return inviteCache;
    }

    public boolean hasInvite(UUID uuid) {
        return inviteCache.containsKey(uuid) && !inviteCache.get(uuid).isEmpty();
    }

    public void acceptInvite(UUID uuid, Island island) {
        if(!hasInvite(uuid)) {
            System.out.println("[Island Manager]: " + uuid.toString() + " doesn't have any invites.");
            return;
        }


    }

    public boolean hasIsland(UUID uuid) {
        Validate.notNull(islandUsers, "[Island Manager]: Island Users map cannot be null.");
        Validate.notNull(islandCache, "[Island Manager]: Island cache map cannot be null.");
        return islandUsers.values().stream().anyMatch(user -> user.getId().equals(uuid)) || islandCache.values().stream().anyMatch(island -> island.getOwner().equals(uuid))
                || islandCache.values().stream().anyMatch(island -> island.getMembers().contains(uuid));
    }

    @EventHandler
    public void debugCommands(PlayerCommandPreprocessEvent event) {
        if(event.getMessage().toLowerCase().startsWith("/icache")) {
            event.setCancelled(true);
            islandCache.values().forEach(island -> event.getPlayer().sendMessage(island.getOwner().toString() + " - "));
        }
    }

}
