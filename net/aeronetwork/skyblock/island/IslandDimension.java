package net.aeronetwork.skyblock.island;

public enum IslandDimension {

    OVERWORLD("Overworld", 0, 0),
    TEST("test", 3, 3);

    String name;
    int color;
    int fade;

    IslandDimension(String name, int color, int fade) {
        this.name = name;
        this.color = color;
        this.fade = fade;
    }

    public String getName() {
        return name;
    }

    public int getColor() {
        return color;
    }

    public int getFade() {
        return fade;
    }
}
